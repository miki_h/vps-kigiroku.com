<?php

namespace Acms\Services\Preview;

use Acms\Contracts\ServiceProvider;
use Acms\Services\Container;
use ACMS_Session;

class PreviewServiceProvider extends ServiceProvider
{
    /**
     * register service
     *
     * @param \Acms\Services\Container $container
     *
     * @return void
     */
    public function register(Container $container)
    {
        $container->singleton('preview', function () {
            $session = ACMS_Session::singleton(array(
                'sess_storage' => 'acms_preview',
            ));
            $lifetime = 60 * 60 * intval(config('url_preview_expire', 48));
            return new Engine($session, $lifetime, BASE_URL . "admin/preview_share/");
        });
    }

    /**
     * initialize service
     *
     * @return void
     */
    public function init()
    {

    }
}