<?php

namespace Acms\Services\Preview;

use Acms\Services\Preview\Contracts\Base;
use ACMS_Session;
use App;
use DB;
use SQL;

class Engine implements Base
{
    /**
     * PHPセッションラッパー
     *
     * @var \ACMS_Session
     */
    protected $session;

    /**
     * @var
     */
    protected $get;

    /**
     * プレビュー共有URLの有効時間
     *
     * @var int
     */
    protected $lifetime;

    /**
     * 共有URL
     *
     * @var string
     */
    protected $shareUrl;

    /**
     * 偽装UAをセッションに保存する時のキー名
     *
     * @var string
     */
    protected $previewFakeUaKeyName = 'preview_fake_ua';

    /**
     *  偽装UAをセッションを確認するためのトークンのキー名
     *
     * @var string
     */
    protected $previewFakeUaTokenKeyName = 'preview_fake_ua_token';

    /**
     * プレビュー共有するための認証トークンのキー名
     *
     * @var string
     */
    protected $previewShareUrlTokenKeyName = 'preview-token';

    /**
     * プレビューモードか判定するために使うURLクエリパラメーター名
     *
     * @var string
     */
    protected $previewModeQueryParameter = 'acms-preview-mode';

    /**
     * Engine constructor.
     *
     * @param ACMS_Session $session
     * @param int $lifetime
     * @param string $shareUrl
     */
    public function __construct(ACMS_Session $session, $lifetime, $shareUrl)
    {
        $app = App::getInstance();
        $this->get = $app->getGetParameter();
        $this->session = $session;
        $this->lifetime = $lifetime;
        $this->shareUrl = $shareUrl;
    }

    /**
     * プレビューモード中か判定
     *
     * @return bool
     */
    public function isPreviewMode()
    {
        $fakeUaToken = $this->session->get($this->previewFakeUaTokenKeyName, false);
        if ($fakeUaToken && $fakeUaToken === $this->get->get($this->previewModeQueryParameter)) {
            setConfig('x_frame_options', 'off');
            return true;
        }
        return false;
    }

    /**
     * 偽装ユーザーエージェントの取得
     *
     * @return string | bool
     */
    public function getFakeUserAgent()
    {
        if ($this->isPreviewMode()) {
            return $this->session->get('preview_fake_ua', false);
        }
        return false;
    }

    /**
     * プレビュー共有モードになれるか判定
     *
     * @return bool
     */
    public function isValidPreviewSharingUrl()
    {
        $previewToken = $this->get->get($this->previewShareUrlTokenKeyName);
        if (empty($previewToken)) {
            return false;
        }
        $SQL = SQL::newSelect('preview_share');
        $SQL->addSelect('preview_share_uri');
        $SQL->addWhereOpr('preview_share_expire', date('Y-m-d H:i:s', REQUEST_TIME), '>=');
        $SQL->addWhereOpr('preview_share_token', $previewToken);
        $url = DB::query($SQL->get(dsn()), 'one');
        if (empty($url)) {
            return false;
        }
        return $this->shareUrlFormat(REQUEST_URL) === $url || (is_ajax() && $this->shareUrlFormat(htmlspecialchars_decode(REFERER, ENT_QUOTES)) === $url);
    }


    /**
     * プレビュー共有URLの取得
     *
     * @param string $url
     * @return string
     */
    public function getShareUrl($url)
    {
        $token = uniqueString() . uniqueString();

        $SQL = SQL::newInsert('preview_share');
        $SQL->addInsert('preview_share_uri', $this->shareUrlFormat($url));
        $SQL->addInsert('preview_share_expire', date('Y-m-d H:i:s', REQUEST_TIME + $this->lifetime));
        $SQL->addInsert('preview_share_token', $token);
        DB::query($SQL->get(dsn()), 'exec');

        return $this->shareUrl . "?token={$token}";
    }

    /**
     * 共有URLで実際に表示するiFrameのURL
     *
     * @return string
     */
    public function getSharePreviewUrl()
    {
        $token = $this->get->get('token');
        if (empty($token)) {
            throw new \RuntimeException('Empty token');
        }
        $SQL = SQL::newSelect('preview_share');
        $SQL->addSelect('preview_share_uri');
        $SQL->addWhereOpr('preview_share_expire', date('Y-m-d H:i:s', REQUEST_TIME), '>=');
        $SQL->addWhereOpr('preview_share_token', $token);

        if ($url = DB::query($SQL->get(dsn()), 'one')) {
            $query = parse_url($url, PHP_URL_QUERY);
            if ($query) {
                $url .= "&preview-token={$token}";
            } else {
                $url .= "?preview-token={$token}";
            }
            return $url;
        }
        throw new \RuntimeException('Failed get preview url.');
    }

    /**
     * 期限切れの共有URLを削除
     */
    public function expiredShareUrl()
    {
        $SQL = SQL::newDelete('preview_share');
        $SQL->addWhereOpr('preview_share_expire', date('Y-m-d H:i:s', REQUEST_TIME - 1), '<');
        DB::query($SQL->get(dsn()), 'exec');
    }

    /**
     * プレビューモードを開始
     *
     * @param string $fakeUserAgent
     * @param string $token
     * @return bool
     */
    public function startPreviewMode($fakeUserAgent, $token)
    {
        if ($fakeUserAgent && $token) {
            $this->session->set($this->previewFakeUaKeyName, $fakeUserAgent);
            $this->session->set($this->previewFakeUaTokenKeyName, $token);
        } else {
            $this->session->delete($this->previewFakeUaKeyName);
            $this->session->delete($this->previewFakeUaTokenKeyName);
        }
        $this->session->save();
    }

    /**
     * プレビューモードを終了
     *
     * @return bool
     */
    public function endPreviewMode()
    {
        $this->session->delete($this->previewFakeUaKeyName);
        $this->session->delete($this->previewFakeUaTokenKeyName);
        $this->session->save();
    }

    /**
     * 共有URLから余分な文字列を削除
     *
     * @param string $url
     * @return string
     */
    protected function shareUrlFormat($url)
    {
        $url = preg_replace('/(\?|&|&amp;)(acms-preview-mode|timestamp|preview-token)=[^&]+/', '', $url);
        return htmlspecialchars_decode($url, ENT_COMPAT);
    }
}
