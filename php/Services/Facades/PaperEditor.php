<?php

namespace Acms\Services\Facades;

class PaperEditor extends Facade
{
    protected static $instance;

    /**
     * @return string
     */
    protected static function getServiceAlias()
    {
        return 'paper-editor';
    }

    /**
     * @return bool
     */
    protected static function isCache()
    {
        return true;
    }
}