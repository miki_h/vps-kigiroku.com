<?php

namespace Acms\Services\PaperEditor;

use Acms\Contracts\ServiceProvider;
use Acms\Services\Container;

class PaperEditorServiceProvider extends ServiceProvider
{
    /**
     * register service
     *
     * @param \Acms\Services\Container $container
     *
     * @return void
     */
    public function register(Container $container)
    {
        $container->singleton('paper-editor', 'Acms\Services\PaperEditor\Helper');
    }

    /**
     * initialize service
     *
     * @return void
     */
    public function init()
    {

    }
}