<?php

class ACMS_GET_Category_EntrySummary extends ACMS_GET_Category_EntryList
{
    var $_axis = array(
        'bid'   => 'self',
        'cid'   => 'self',
    );
    
    var $_endGluePoint = null;
    
    function initVars()
    {
        $config = array(
            'categoryOrder'             => config('category_entry_summary_category_order'),
            'categoryEntryListLevel'    => config('category_entry_summary_level'),
            'categoryIndexing'          => config('category_entry_summary_category_indexing'),
            'entryAmountZero'           => config('category_entry_summary_entry_amount_zero'),
            'order'                     => config('category_entry_summary_order'),
            'limit'                     => intval(config('category_entry_summary_limit')),
            'offset'                    => intval(config('category_entry_summary_offset')),
            'indexing'                  => config('category_entry_summary_indexing'),
            'secret'                    => config('category_entry_summary_secret'),
            'notfound'                  => config('mo_category_entry_summary_notfound'),
            'noimage'                   => config('category_entry_summary_noimage'),
            'unit'                      => config('category_entry_summary_unit'),
            'newtime'                   => config('category_entry_summary_newtime'),
            'imageX'                    => intval(config('category_entry_summary_image_x')),
            'imageY'                    => intval(config('category_entry_summary_image_y')),
            'imageTrim'                 => config('category_entry_summary_image_trim'),
            'imageZoom'                 => config('category_entry_summary_image_zoom'),
            'imageCenter'               => config('category_entry_summary_image_center'),
            'mainImageOn'               => config('category_entry_summary_image_on'),
            'categoryLoopClass'         => config('category_entry_summary_category_loop_class'),
            'fulltextWidth'             => config('category_entry_summary_fulltext_width'),
            'fulltextMarker'            => config('category_entry_summary_fulltext_marker'),
            'loop_class'                => config('category_entry_summary_entry_loop_class'),

            'entryFieldOn'              => config('category_entry_summary_entry_field_on'),
            'categoryInfoOn'            => 'on',
            'categoryFieldOn'           => config('category_entry_summary_category_field_on'),
            'userInfoOn'                => 'on',
            'userFieldOn'               => config('category_entry_summary_user_field_on'),
            'blogInfoOn'                => 'on',
            'blogFieldOn'               => config('category_entry_summary_blog_field_on'),
        );
        if(!empty($this->order)){$config['order'] = $this->order;}
        
        return $config;
    }
    
    function buildQuery($cid, &$Tpl)
    {
        $BlogSub        = null;
        $CategorySub    = null;

        $DB     = DB::singleton(dsn());
        $SQL    = SQL::newSelect('entry');
        $SQL->addLeftJoin('category', 'category_id', 'entry_category_id');
        $SQL->addLeftJoin('blog', 'blog_id', 'entry_blog_id');

        // category
        $SUB = SQL::newWhere();
        $SUB->addWhereOpr('entry_category_id', $cid, '=', 'OR');

        $SUB2 = SQL::newSelect('entry_sub_category');
        $SUB2->addSelect('entry_sub_category_eid');
        $SUB2->addWhereOpr('entry_sub_category_id', $cid);
        $SUB->addWhereIn('entry_id',  DB::subQuery($SUB2), 'OR');

        $SQL->addWhere($SUB);

        if ( !empty($this->bid) ) {
            $BlogSub = SQL::newSelect('blog');
            $BlogSub->setSelect('blog_id');
            ACMS_Filter::blogTree($BlogSub, $this->bid, $this->blogAxis());

            if ( 'on' === $this->_config['secret'] ) {
                ACMS_Filter::blogDisclosureSecretStatus($BlogSub);
            } else {
                ACMS_Filter::blogStatus($BlogSub);
            }
        }
        if ( $uid = intval($this->uid) ) {
            $SQL->addWhereOpr('entry_user_id', $this->uid);
        }

        if ( !empty($this->eid) ) {
            $SQL->addWhereOpr('entry_id', $this->eid);
        }
        ACMS_Filter::entrySpan($SQL, $this->start, $this->end);
        ACMS_Filter::entrySession($SQL);

        if ( !empty($this->tags) ) {
            ACMS_Filter::entryTag($SQL, $this->tags);
        }
        if ( !empty($this->keyword) ) {
            ACMS_Filter::entryKeyword($SQL, $this->keyword);
        }
        if ( !empty($this->Field) ) {
            if ( config('category_entry_summary_field_search') == 'entry' ) {
                ACMS_Filter::entryField($SQL, $this->Field);
            } else {
                ACMS_Filter::categoryField($SQL, $this->Field);
            }
        }
        if ( 'on' == $this->_config['indexing'] ) {
            $SQL->addWhereOpr('entry_indexing', 'on');
        }
        if ( 'on' <> $this->_config['noimage'] ) {
            $SQL->addWhereOpr('entry_primary_image', null, '<>');
        }

        //-------------------------
        // filter (blog, category) 
        if ( $BlogSub ) {
            $SQL->addWhereIn('entry_blog_id', $DB->subQuery($BlogSub));
        }
        $limit  = $this->_config['limit'];
        $offset = intval($this->_config['offset']);
        if ( 1 > $limit ) return '';

        $sortFd = ACMS_Filter::entryOrder($SQL, $this->_config['order'], $this->uid, $cid);
        $SQL->setLimit($limit, $offset);

        if ( !empty($sortFd) ) {
            $SQL->setGroup($sortFd);
        }
        $SQL->addGroup('entry_id');

        $q  = $SQL->get(dsn());

        $all    = $DB->query($q, 'all');
        if ( empty($all) ) {
            return false;
        }
        $this->_endGluePoint = count($all);

        return $q;
    }

    function preBuildUnit()
    {
        $entryIds = array();
        $blogIds = array();
        $userIds = array();
        $categoryIds = array();

        foreach ($this->entries as $entry) {
            if (!empty($entry['entry_id'])) $entryIds[] = $entry['entry_id'];
            if (!empty($entry['entry_blog_id'])) $blogIds[] = $entry['entry_blog_id'];
            if (!empty($entry['entry_user_id'])) $userIds[] = $entry['entry_user_id'];
            if (!empty($entry['entry_category_id'])) $categoryIds[] = $entry['entry_category_id'];
        }

        // メイン画像のEagerLoading
        if (!isset($this->_config['mainImageOn']) || $this->_config['mainImageOn'] === 'on') {
            $this->eagerLoadingData['mainImage'] = Tpl::eagerLoadMainImage($this->entries);
        }
        // フルテキストのEagerLoading
        if (!isset($this->_config['fullTextOn']) || $this->_config['fullTextOn'] === 'on') {
            $this->eagerLoadingData['fullText'] = Tpl::eagerLoadFullText($entryIds);
        }
        // タグのEagerLoading
        $this->eagerLoadingData['tag'] =  Tpl::eagerLoadTag($entryIds);
        // エントリーフィールドのEagerLoading
        if (!isset($this->_config['entryFieldOn']) || $this->_config['entryFieldOn'] === 'on') {
            $this->eagerLoadingData['entryField'] = eagerLoadField($entryIds, 'eid');
        }
        // ユーザーフィールドのEagerLoading
        if (isset($this->_config['userInfoOn']) && $this->_config['userInfoOn'] === 'on') {
            $this->eagerLoadingData['userField'] = eagerLoadField($userIds, 'uid');
        }
        // ブログフィールドのEagerLoading
        if (isset($this->_config['blogInfoOn']) && $this->_config['blogInfoOn'] === 'on') {
            $this->eagerLoadingData['blogField'] = eagerLoadField($blogIds, 'bid');
        }
        // カテゴリーフィールドのEagerLoading
        if (isset($this->_config['categoryInfoOn']) && $this->_config['categoryInfoOn'] === 'on') {
            $this->eagerLoadingData['categoryField'] = eagerLoadField($categoryIds, 'cid');
        }
        // サブカテゴリーのEagerLoading
        if (isset($this->_config['categoryInfoOn']) && $this->_config['categoryInfoOn'] === 'on') {
            $this->eagerLoadingData['subCategory'] = eagerLoadSubCategories($entryIds);
        }
    }

    function buildUnit($eRow, &$Tpl, $cid, $level, $count = 0)
    {
        $this->buildSummary($Tpl, $eRow, $count, $this->_endGluePoint, $this->_config, array(), $this->eagerLoadingData);
    }
}
