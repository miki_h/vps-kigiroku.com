<?php

class ACMS_GET_Api_Instagram_AdminSelectBusinessAccount extends ACMS_GET
{
  function get()
  {
      $instagram = App::make('instagram-login');
      $Tpl    = new Template($this->tpl, new ACMS_Corrector());
      if ( !!($accessToken = $instagram->loadAccessToken(BID)) ) {
          $instagram->setAccessToken($accessToken);
          $instagram->setMe();
          $accounts = $instagram->getBussinessAccounts();
          return $Tpl->render(array(
            'accounts' => $accounts
          ));
      }
      return $Tpl->render(array(
        'error' => 'oAuth認証が必要です。'
      ));
  }
}