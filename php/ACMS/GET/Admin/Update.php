<?php

use Acms\Services\Update\Engine;
use Acms\Services\Update\System\CheckForUpdate;
use Acms\Services\Facades\Storage;

class ACMS_GET_Admin_Update extends ACMS_GET_Admin
{
    function get()
    {
        if ( 'update' <> ADMIN ) return false;
        // ToDo: Limited Administrator & root user

        $Tpl = new Template($this->tpl, new ACMS_Corrector());
        $checkUpdateService = App::make('update.check');
        $logger = App::make('update.logger');
        $updateService = new Engine($logger);

        $rootVars = array(
            'finalCheckTime' => date('Y/m/d H:i:s', $checkUpdateService->getFinalCheckTime()),
        );
        $dbVars = array(
            'databaseVersion' => $updateService->databaseVersion,
            'systemVersion' => $updateService->systemVersion,
        );

        try {
            /**
             * システムアップデート中チェック
             */
            if ( Storage::exists($logger->getDestinationPath()) ) {
                $rootVars['processing'] = 1;
            } else {
                $rootVars['processing'] = 0;
            }

            /**
             * データベースのバージョンチェック
             */
            $check = $updateService->checkUpdates();
            if ( $check ) {
                $Tpl->add('update', $dbVars);
            } else {
                if ($updateService->compareDatabase()) {
                    $Tpl->add('match', $dbVars);
                } else {
                    $dbVars['diffDB'] = '1';
                    $Tpl->add('update', $dbVars);
                }
            }

            /**
             * システムのバージョンチェック
             */
            $range = CheckForUpdate::PATCH_VERSION;
            if ( config('system_update_range') === 'minor' ) {
                $range = CheckForUpdate::MINOR_VERSION;
            }
            if ( $checkUpdateService->checkUseCache(phpversion(), $range) ) {
                $version = $checkUpdateService->getUpdateVersion();
                if (!$this->editionCheck($version)) {
                    $Tpl->add('pro_edition', array());
                }
                $Tpl->add('old_version', array(
                    'version' => $version,
                    'downloadUrl' => $checkUpdateService->getPackageUrl(),
                ));
                if ($versions = $checkUpdateService->getReleaseNote()) {
                    foreach ($versions as $version) {
                        foreach ($version->logs->features as $message) {
                            $Tpl->add(array('feature:loop', 'version:loop', 'changelog'), array(
                                'log' => $message,
                            ));
                        }
                        foreach ($version->logs->changes as $message) {
                            $Tpl->add(array('change:loop', 'version:loop', 'changelog'), array(
                                'log' => $message,
                            ));
                        }
                        foreach ($version->logs->fix as $message) {
                            $Tpl->add(array('fix:loop', 'version:loop', 'changelog'), array(
                                'log' => $message,
                            ));
                        }
                        $Tpl->add(array('version:loop', 'changelog'), array(
                            'version' => $version->version,
                            'alert' => $version->alert,
                        ));
                    }
                    $Tpl->add('changelog', array(
                        'url' => $checkUpdateService->getChangelogUrl(),
                    ));
                }
            } else {
                if ( config('system_update_range') === 'minor' ) {
                    $Tpl->add('latest:minor', array());
                } else {
                    $Tpl->add('latest:patch', array());
                }
            }

        } catch ( \Exception $e ) {
            $Tpl->add('error', $dbVars);
        }

        $Tpl->add(null, $rootVars);

        return $Tpl->get();
    }

    protected function editionCheck($version)
    {
        list($major, $minor) = explode('.', $version);
        $nextVersion = intval($major) * 100 + intval($minor);
        if (intval(LICENSE_SYSTEM_MAJOR_VERSION) < $nextVersion) {
            return false;
        }
        return true;
    }
}
