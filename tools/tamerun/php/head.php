    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:title" content="" />
    <meta property="og:title" content="ためルン" />
 	<meta property="og:image" content="" />
	<meta property="og:type" content="" />
    <meta property="og:description" content="" />
    <meta name="description" content="" />
    <meta property="og:description" content="" />
    <meta name="keywords" content="" />
    
    <link rel="shortcut icon" href="" >
    <link rel="stylesheet" href="/tamerun/css/fullcalendar.min.css">
    <link rel="stylesheet" href="/tamerun/css/column.css">
    <link rel='stylesheet' href='/tamerun/css/hiraku.css'>
    <link rel="stylesheet" href="/tamerun/css/style.css">
    <link rel="stylesheet" type="text/css" href="/tamerun/php/options.php" /> 
