$('#calendar').fullCalendar({
			header: {
				//それぞれの位置に設置するボタンやタイトルをスペース区切りで指定できます。指定しない場合、非表示にできます。
				// 'title'→月・週・日のそれぞれの表示に応じたタイトル
				// 'prev'→前へボタン
				// 'next'→次へボタン
				// 'today'→当日表示ボタン
				left: 'prev', //左側に配置する要素
				center: 'title', //中央に配置する要素
				right: 'next' //右側に配置する要素
			},
			// timeFormat: { // for event elements
			// 	'': 'H:mm' // default
			// },
			// axisFormat: 'H:mm',
			// timeFormat: {
			// 	agenda: 'H:mm{ - H:mm}'
			// },
			timeFormat: 'H(:mm)',
	        // 列の書式
	        // columnFormat: {
	        //     month: 'ddd',    // 月
	        //     week: "d'('ddd')'", // 7(月)
	        //     day: "d'('ddd')'" // 7(月)
	        // },
	        // タイトルの書式
	        titleFormat: 'YYYY年 MMMM',
		        // month: 'yyyy年 M月',
		        // week: '[yyyy年 ]M月 d日{ —[yyyy年 ][ M月] d日}',
		        // day: 'yyyy年 M月 d日 dddd'
		    // 月名称
	        monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	        // 曜日名称
	        dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
	        // 曜日略称
	        dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
	        // 選択可
		});