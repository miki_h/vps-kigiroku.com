$(function(){
    $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
        default_offset_pct: 0.5, // スクロールバーの初期位置の設定。0が左、0.5で中央、1で右となります。verticalの場合には上から0、下が1。
        orientation: 'horizontal', // スクロールする方向の設定。初期値ではhorizontal
        before_label: '青い池', // マウスオーバー時の初期値beforeと書かれた部分のラベル名
        after_label: '青い池横の自然', // マウスオーバー時の初期値afterと書かれた部分のラベル名
        no_overlay: false, //マウスオーバー時のbefore、afterのオーバーレイコンテンツ表示。初期値はfalse、trueで非表示。
        move_with_handle_only: false, // クリック、タップした場所からスクロールできるようにするかどうかの設定。初期値はtrueでスクロールが不可、falseにするとクリックタップした場所からスクロールが可能に
        click_to_move: true // クリックとタップでスクロールバーを移動させるかどうかの設定。初期値はfalse
    });
    $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
        default_offset_pct: 0.5, // スクロールバーの初期位置の設定。0が左、0.5で中央、1で右となります。
        orientation: 'vertical', // Orientation of the before and after images ('horizontal' or 'vertical')
        move_with_handle_only: false, // Allow a user to swipe anywhere on the image to control slider movement.
    });
});
