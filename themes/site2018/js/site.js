// ページ上部へ戻るボタンの表示の設定
$(function () {
    var $nav = $('.page-top-btn');
    var offset  = '50';
    var footer  = $(document).height() - $(window).height() - 500;
    $(window).scroll(function() {
        if($(window).scrollTop() > offset) {
            $nav.addClass('page-top-btn-appear');
        } else {
            $nav.removeClass('page-top-btn-appear');
        }
    });

    // 郵便番号の「-」の挿入
    $('.js-insert-hyphen').blur(function(){
        function insertStr(str, index, insert) {
            return str.slice(0, index) + insert + str.slice(index, str.length);
        }
        var zip = $(this).val();
        if (!zip.match('-')) {
            var zipHyphen = insertStr(zip, 3, '-');
            $(this).val(zipHyphen);
        }
    });
});

// オフキャンバスのブレイクポイントの設定
ACMS.Ready(function(){
    ACMS.Config.offcanvas.breakpoint = 1023;
});
