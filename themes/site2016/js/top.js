// トップページのスライダーの設定
$(function() {
	$('.js-slider').slick({
		accessibility: true,
		dots: true,
		arrows: true,
		prevArrow: '<button type="button" class="slick-prev">前へ</button>',
		nextArrow: '<button type="button" class="slick-next">次へ</button>',
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnHover : false
	});

	// スライダーの内容を通達する時をフォーカスしている時のみに変更
	var $slickList = $('.slick-list');
	$slickList.removeAttr('aria-live');

	$slickList.on('focus', function() {
	    $slickList.attr('aria-live', 'polite');
	});
	$slickList.on('blur', function() {
	    $slickList.removeAttr('aria-live', 'polite');
	});

	// autoPlay:true時に使用する再生・停止ボタンを追加（autoPlayを使用しない場合は削除してください）
	var $pauseBtn = $('<button type="button" id="autoplay-btn" aria-live="polite"><span class="js-slider-text acms-hide-visually">スライダーを自動再生を停止する</span></button>');
	$pauseBtn.prependTo($('.js-slider'));
	var $innerText = $('.js-slider-text')

	$('.autoplay-btn-play:first-child').hide();
	var play = true;
	// 停止ボタンをクリックした時の処理
	$('#autoplay-btn').click(function(){
		$pauseBtn.toggleClass('active');
		if(play === true) {
			$('.js-slider').slick('slickPause');
            $innerText.text('スライダーを自動再生する');
			play = false;
		} else {
			$('.js-slider').slick('slickPlay');
            $innerText.text('スライダーの自動再生を停止する');
			play = true;
		}
	});
});
