import React, { Component, CSSProperties, ReactNode } from 'react';
import classnames from 'classnames';
import styled from 'styled-components';

const NotifyBox = styled.div`
  background: #5690d8;
  border-radius: 0;
  bottom: auto;
  color: #fff;
  font-size: 13px;
  left: 0;
  padding: 10px;
  position: fixed;
  text-align: center;
  right: 0;
  top: -70px;
  transition: top .2s ease-in;
  transform: translate3d(0px, 0px, 200px);
  width: 100%;
  z-index: 100100;
  &.active {
    top: 0;
  }
`;

interface NotifyProp {
  onFinish: Function,
  style?: CSSProperties,
  message: ReactNode,
  show: boolean
}

interface NotifyState {
  show: boolean,
  active: boolean
}

export default class Notify extends Component<NotifyProp, NotifyState> {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      active: false
    };
  }

  componentWillReceiveProps(props) {
    const { onFinish } = this.props;
    if (props.show === true) {
      setTimeout(() => {
        this.setState({
          active: true
        });
      }, 1);
      setTimeout(() => {
        this.setState({
          active: false
        });
      }, 1600);
      setTimeout(() => {
        if (onFinish) {
          onFinish();
        }
      }, 1100);
    }
  }

  render() {
    const { message, style } = this.props;
    const { active } = this.state;
    return (<NotifyBox style={style} className={classnames({ active })} >{message}</NotifyBox>);
  }
}
