import React from 'react';
import { render } from 'react-dom';
import Media from "../components/editor/media";
import Editor from '../components/editor/paper-editor';
import {
  Paragraph,
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
  Heading6,
  ListItem,
  Emphasis,
  OrderedList,
  BulletList,
  Blockquote,
  Code,
  Table,
  DefaultKeys,
  DefaultPlugins,
  Trash,
  MoveDown,
  MoveUp,
  Link,
  Strike,
  Strong,
  Underline,
  Embed,
  CustomBlock,
  CustomMark,
  Heading1Icon,
  Heading2Icon,
  Heading3Icon,
  Heading4Icon,
  Heading5Icon,
  Heading6Icon,
} from 'paper-editor';

const Extensions = {
  Paragraph,
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
  Heading6,
  ListItem,
  BulletList,
  OrderedList,
  Blockquote,
  Code,
  Table,
  Media,
  Emphasis,
  Underline,
  Strike,
  Link,
  MoveDown,
  MoveUp,
  Trash,
  Strong,
  Embed,
  DefaultKeys,
  DefaultPlugins,
  CustomBlock,
  CustomMark
};

const icons = {
  Heading1Icon: <Heading1Icon style={{ width: "24px", height: "24px" }} />,
  Heading2Icon: <Heading2Icon style={{ width: "24px", height: "24px" }} />,
  Heading3Icon: <Heading3Icon style={{ width: "24px", height: "24px" }} />,
  Heading4Icon: <Heading4Icon style={{ width: "24px", height: "24px" }} />,
  Heading5Icon: <Heading5Icon style={{ width: "24px", height: "24px" }} />,
  Heading6Icon: <Heading6Icon style={{ width: "24px", height: "24px" }} />,
}

export default (editor: HTMLElement) => {

  const editorEdit = editor.querySelector<HTMLElement>(ACMS.Config.PaperEditorEditMark);
  const editorBody = editor.querySelector<HTMLInputElement>(ACMS.Config.PaperEditorBodyMark);
  const editorTitle = editor.querySelector<HTMLInputElement>(ACMS.Config.PaperEditorTitleMark);

  const html = editorBody ? editorBody.value : '';
  const title = editorTitle ? editorTitle.value : '';
  const { useTitle } = editor.dataset;


  render(<Editor
    useTitle={useTitle === 'true'}
    html={html}
    title={title}
    minHeight={ACMS.Config.PaperEditorUnitMinHeight}
    maxHeight={ACMS.Config.PaperEditorUnitMaxHeight}
    extensions={ACMS.Config.PaperEditorConf(Extensions, editor, icons)}
    replacements={ACMS.Config.PaperEditorReplace(Extensions)}
    removes={ACMS.Config.PaperEditorRemoves}
    adds={ACMS.Config.PaperEditorAdds(Extensions)}
    onChange={(body) => {
      if (editorTitle) {
        editorTitle.value = body.title;
      }
      editorBody.value = JSON.stringify(body);
    }}
  />, editorEdit);
};
