<?php

ob_start(); //バッファリング開始

// Bitbucket, Applepleからのリクエストのみ応じる
// The Public IP addresses for these hooks are: 54.248.107.22.
if ( !in_array($_SERVER['REMOTE_ADDR'], array('104.192.143.1', '104.192.143.2', '104.192.143.3', '104.192.143.65', '104.192.143.66', '104.192.143.67')) ) {
    die;
}

chdir('/var/www/kigiroku.com/');
echo shell_exec("git pull origin master");

// ログ出力
$output = '【'.date('Y-m-d H:i:s').'】'.ob_get_flush().PHP_EOL;
ob_end_clean();

$fp = fopen('/var/www/kigiroku.com/git/githook_log.txt', 'a');
fwrite($fp, $output);
fclose($fp);